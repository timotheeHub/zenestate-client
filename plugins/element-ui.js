import Vue from 'vue';
import { DatePicker } from 'element-ui';
import lang from 'element-ui/lib/locale/lang/en';
import locale from 'element-ui/lib/locale';
import 'element-ui/lib/theme-default/index.css';

locale.use(lang);
Vue.component(DatePicker.name, DatePicker);
